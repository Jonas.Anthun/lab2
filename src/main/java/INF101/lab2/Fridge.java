package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javafx.css.Size;

public class Fridge implements IFridge {
    int maxCapacity = 20;
    private List<FridgeItem> items;

    public Fridge() {
        items = new ArrayList<FridgeItem>();

    }
    

    @Override
    public int nItemsInFridge() {

        return items.size();
    }

    @Override
    public int totalSize() {
        
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (item == null){
            throw new IllegalArgumentException();
        }
        if (items.size() >= maxCapacity){
            return false;
        }
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (item == null){
            throw new IllegalArgumentException("Cannot take out null from fridge");
        }
        if (!items.contains(item)){
            throw new NoSuchElementException();
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem item : expiredItems) {
            items.remove(item);
        }
        return expiredItems;
    }
    
}
